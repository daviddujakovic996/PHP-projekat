window.onload = function () {
    


   
        $.ajax({
            url: "models/Admin/dohvatiSveParfeme.php",
            method: "POST",
            dataType: "json",
            data: {
                send: true,

            },
            success: function (data) {

                popuniPodatke(data);
                prikaziPaginaciju(data[data.length - 1]);

            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
    })


    document.getElementById("insert").addEventListener("click", function (e) {
        e.preventDefault;

        var naziv = document.getElementById("naziv").value;

        var cena = document.getElementById("cena").value;

        var p = document.getElementById("pol");
        var pol = p.options[p.selectedIndex].value;

        var b = document.getElementById("brend");
        var brend = b.options[b.selectedIndex].value;

        var gi = document.getElementById("gi");
        var godinaIzdanja = gi.options[gi.selectedIndex].value;

        var m = document.getElementById("militraza");
        var militraza = m.options[m.selectedIndex].value;

       

        $.ajax({
            url: "models/Admin/insertParfem.php",
            method: "POST",
            dataType: "json",
            data: {
                send: true,
                naziv: naziv,
                cena: cena,
                pol: pol,
                brend: brend,
                godinaIzdanja: godinaIzdanja,
                militraza: militraza,
                
            },
            success: function (data) {

               // alert("uspesno ste uneli proizvod");
                popuniPodatke(data);
                prikaziPaginaciju(data[data.length - 1]);
            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        })
    })
    
    function popuniPodatke(podaci) {
        let ispis = "";
        for (let p of podaci) {
            if (p.idParfem == undefined) {
                break;
            }
            ispis += `
 <tr class="text-center">
						        <td class="product-remove"><a href="#" class="izbrisi" data-id="${p.idParfem}"><span class="ion-ios-close"></span></a></td>
						        
						        <td class="image-prod"><div class="img" style="background-image:url(assets/images/${p.Putanja});"></div></td>
						        
						        <td class="product-name">
						        	<h3>${p.Naziv}</h3>
						        	
						        </td>
						        
						        <td class="price">${p.Cena}</td>
						        
						        <td class="quantity">
						        	<div class="input-group mb-3">
					             	<input type="text" name="quantity" class="quantity form-control input-number" value="${p.brojKupovina}" min="1" max="100" disabled>
					          	</div>
					          </td>
                                <td>
						        <div class="input-group mb-3">
					             	<a href="index.php?page=Moj Nalog&idUpdate=${p.idParfem}" name="update"  class="btn btn-primary ">Update</a>
						        </div></td>
						      </tr><!-- END TR-->
`
        }
        if (ispis == "") {
            ispis += "<h2>NEMA NARUCENIH PARFEMA!</h2>";


        }

        document.getElementsByTagName("tbody")[0].innerHTML = ispis;

    }

    function prikaziPaginaciju(brojProizvoda) {
        let brojPoStrani = 6;
        let ukupnoStranica = brojProizvoda / brojPoStrani;
        let ispis = "<ul><li><a href='#' class='pagin' data-id='1'> << </a></li>";

        for (let i = 1; i < ukupnoStranica + 1; i++) {
            ispis += "<li><a href='#' class='pagin' data-id='" + i + "'>" + i + " </a></li>";
        }

        ispis += "<li><a href='#' class='pagin' data-id='" + Math.round(ukupnoStranica) + "'> >> </a></li></ul>";
        document.getElementById("paginacija").innerHTML = ispis;

        $(".pagin").click(function (e) {
            e.preventDefault();


            var pagin = $(this).data("id");
            var paginUInt = parseInt(pagin);

            $.ajax({
                url: "models/Admin/dohvatiSveParfeme.php",
                method: "POST",
                data: { send: true, strana: paginUInt },
                dataType: "json",
                success: function (data) {
                    
                    popuniPodatke(data);

                }
            });
        })
    }

   
    $(document).on("click", ".izbrisi", function (e) {
        e.preventDefault();
        var idParfem = $(this).data("id");
       
        $.ajax({
            url: "models/Admin/izbrisiParfem.php",
            method: "POST",
            data: {
                parfem: idParfem,
                send: true
            },
            dataType: "json",
            success: function (data) {
               

                popuniPodatke(data);
                prikaziPaginaciju(data[data.length - 1]);


            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        })
    })
}




    


