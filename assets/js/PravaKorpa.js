window.onload = function (e) {
    e.preventDefault;

    $.ajax({
        url: "models/korpa/selectAll.php",
        method: "POST",
        data: {
          
            send: true
        },
        success: function (data) {

           popuniPodatke(JSON.parse(data));
            //alert(JSON.parse(data));
            //var zbir = 0;
            var element = document.getElementsByName("total");
            var zbir = 0;
            for (var i = 0; i < element.length; i++) {
                zbir += parseInt(element[i].value);
               
            }
            document.getElementById("zbir").innerHTML = zbir;
           
        },
        error: function (xhr, status, error) {

            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })

    document.getElementById("naruci").addEventListener("click", function () {
      
        $.ajax({
            url: "models/korpa/naruci.php",
            method: "POST",
            data: {

                send: true
            },
            success: function () {

                alert("USPESNO STE NARUCILI PROIZVODE");
                window.location = "index.php";
            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        })
    })

};

function popuniPodatke(podaci) {
    let ispis = "";
    for (let p of podaci) {
        
        ispis += `
 <tr class="text-center">
						        <td class="product-remove"><a href="#" onclick="obrisi(${p.IdParfem})"><span class="ion-ios-close"></span></a></td>
						        
						        <td class="image-prod"><div class="img" style="background-image:url(assets/images/${p.Putanja});"></div></td>
						        
						        <td class="product-name">
						        	<h3>${p.Naziv}</h3>
						        	
						        </td>
						        
						        <td class="price">${p.Cena}</td>
						        
						        <td class="quantity">
						        	<div class="input-group mb-3">
					             	<input type="text" name="quantity" class="quantity form-control input-number" value="${p.Kolicina}" min="1" max="100" disabled>
					          	</div>
					          </td>
                                <td>
						        <div class="input-group mb-3">
					             	<input type="text" name="total" id="total" class="total form-control input-number" value="${p.UkupnaCena}" min="1" max="100" disabled>
						        </div></td>
						      </tr><!-- END TR-->
`
    }
    if (ispis == "") {
        ispis += "<h2>NEMA PARFEMA U KORPI!</h2>";


    }
    document.getElementsByTagName("tbody")[0].innerHTML = ispis;

}
function obrisi(idParfem) {
  
       
        var parfem = idParfem;
        $.ajax({
            url: "models/korpa/removeFromKorpa.php",
            method: "POST",
            data: {
                parfem: parfem,
                send: true
            },
            success: function (data) {

                popuniPodatke(JSON.parse(data));
                //alert(JSON.parse(data));
                //var zbir = 0;
                var element = document.getElementsByName("total");
                var zbir = 0;
                for (var i = 0; i < element.length; i++) {
                    zbir += parseInt(element[i].value);

                }
                document.getElementById("zbir").innerHTML = zbir;

            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        })
    
}