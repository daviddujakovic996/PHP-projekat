
     
    $(document).ready(function () {
        
        

        
        

           
        filter_data();
            function filter_data() {
                $('.product').html('<div id="loading" style="" ></div>');
                var action = 'fetch_data';
                var minimum_price = $('#hidden_minimum_price').val();
                var maximum_price = $('#hidden_maximum_price').val();
                var brand = get_filter('brend');
                var pol = get_filter('pol');
                var militraza = get_filter('militraza');
                var godina = get_filter('godina');

                var pagin = $(this).data("id");
                var paginUInt = parseInt(pagin);

                $.ajax({
                    url: "models/shopProducts/filterProducts.php",
                    method: "POST",
                    data: { action: action, minimum_price: minimum_price, maximum_price: maximum_price, brand: brand, pol: pol, militraza: militraza, godina: godina, strana: paginUInt },
                    dataType:"json",
                    success: function (data) {
                        //alert(JSON.parse(data));
                        // prikazi(JSON.parse(data));
                        //alert(data[data.length - 1]);
                       // let popped = data[data.length - 1].pop();
                       // document.getElementById("ispis").innerHTML+=data;
                       // data.slice(0, -1); 
                        prikazi(data);
                        prikaziPaginaciju(data[data.length - 1]);
                       // $("#ispis").html(data);
                    }
                });

        }
        
      
            function get_filter(class_name) {
                var filter = [];
                $('.' + class_name + ':checked').each(function () {
                    filter.push($(this).val());
                });
                return filter;
            }

            $('.common_selector').click(function () {
                filter_data();
        });

    
        $("input[type=number]").keyup(function () {
            filter_data();
        })
       
        function prikazi(podaci) {
            
            let ispis = "";
            for (let p of podaci) {
                if (p.idParfem == undefined) {
                    break;
                }


                    ispis += `       
<div class="col-sm col-md-6 col-lg ftco-animate" >
    				<div class="product">
						
						<a href="index.php?page=singleProduct&product=${p.idParfem}" class="img-prod"><img class="img-fluid" src="assets/images/${p.Putanja}" alt="${p.Naziv}">
                        
                

                       
						</a>
    					<div class="text py-3 px-3">
    						<h3><a href="index.php?page=singleProduct&product=${p.idParfem}">${p.Naziv}</a></h3>
    						<div class="d-flex">
    							<div class="pricing">
			    							<p class="price"><span class="mr-2 price-dc" id="cena">

               </span>
											<span class="price-sale">
               
                   ${p.Cena}  " RSD"
               
                

											</span></p>
			    						</div>
										<div class="rating">
			    							<p class="text-right">
			    								
			    							</p>
			    						</div>
	    					</div>
							
	    					<hr>
    						
    					</div>
    			</div>	
</div>
 `;
                
                }
            if (ispis == "") {
                ispis += "<h2>NEMA PODATAKA ZA TAKAV IZBOR!</h2>";
                

            }
            document.getElementById("ispis").innerHTML = ispis;

            
        }

        function prikaziPaginaciju(brojProizvoda) {
            let brojPoStrani = 6;
            let ukupnoStranica = brojProizvoda / brojPoStrani;
            let ispis = "<ul><li><a href='#' class='pagin' data-id='1'> << </a></li>";

            for (let i = 1; i < ukupnoStranica + 1; i++) {
                ispis +="<li><a href='#' class='pagin' data-id='"+i+"'>"+ i +" </a></li>";
            }

            ispis += "<li><a href='#' class='pagin' data-id='"+ Math.round(ukupnoStranica) + "'> >> </a></li></ul>";
            document.getElementById("paginacija").innerHTML = ispis;

            $(".pagin").click(function (e) {
                e.preventDefault();
                $('.product').html('<div id="loading" style="" ></div>');
                var action = 'fetch_data';
                var minimum_price = $('#hidden_minimum_price').val();
                var maximum_price = $('#hidden_maximum_price').val();
                var brand = get_filter('brend');
                var pol = get_filter('pol');
                var militraza = get_filter('militraza');
                var godina = get_filter('godina');

                var pagin = $(this).data("id");
                var paginUInt = parseInt(pagin);

                $.ajax({
                    url: "models/shopProducts/filterProducts.php",
                    method: "POST",
                    data: { action: action, minimum_price: minimum_price, maximum_price: maximum_price, brand: brand, pol: pol, militraza: militraza, godina: godina, strana: paginUInt },
                    dataType: "json",
                    success: function (data) {
                        //alert(JSON.parse(data));
                        // prikazi(JSON.parse(data));
                        //alert(data[data.length - 1]);
                        // let popped = data[data.length - 1].pop();
                        // document.getElementById("ispis").innerHTML+=data;
                        // data.slice(0, -1); 
                        prikazi(data);
                        // prikaziPaginaciju(data[data.length - 1]);
                        // $("#ispis").html(data);
                    }
                });
            })
        }

        
});







