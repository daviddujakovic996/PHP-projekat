window.onload = function () {
    
    document.getElementById("update").addEventListener("click", function (e) {
        e.preventDefault();
       
        var naziv = document.getElementById("naziv").value;
       
        var cena = document.getElementById("cena").value;
        
    var p = document.getElementById("pol");
        var pol = p.options[p.selectedIndex].value;
     
        var b = document.getElementById("brend");   
        var brend = b.options[b.selectedIndex].value;
      
        var gi = document.getElementById("gi");
        var godinaIzdanja = gi.options[gi.selectedIndex].value;
      
        var m = document.getElementById("militraza");
        var militraza = m.options[m.selectedIndex].value;
       
        var idParfem = document.getElementById("skriveno").value;
        
    $.ajax({
        url: "models/Admin/updateParfem.php",
        method: "POST",
        dataType: "json",
        data: {
            send: true,
            naziv: naziv,
            cena: cena,
            pol: pol,
            brend: brend,
            godinaIzdanja: godinaIzdanja,
            militraza: militraza,
            idParfem: idParfem
        },
        success: function () {
            
            alert("uspesno ste izmenili proizvod");

        },
        error: function (xhr, status, error) {
            
            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takav proizvod"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
        })
    })
}