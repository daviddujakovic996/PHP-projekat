<?php
session_start();
include('../../config/connection.php');

if(isset($_POST["send"]))
{
$idParfem=$_POST["parfem"];
$idKorisnik=$_SESSION["korisnik"]->idKorisnik;
$upit="SELECT k.Idkorisnik,k.IdParfem,k.Kolicina,k.UkupnaCena,p.Naziv,s.Putanja,p.Cena from korpa k INNER JOIN parfem p ON k.IdParfem=p.idParfem INNER JOIN slika s ON p.idSlika=s.idSlika WHERE k.IdKorisnik=:id AND k.Narucen=0";
$obrisi="DELETE FROM korpa WHERE IdParfem=:idp AND IdKorisnik=:idk";
        try{
			
            $priprema = $conn->prepare($obrisi);
            $priprema->bindParam(":idp",$idParfem);
            $priprema->bindParam(":idk",$idKorisnik);
			 $priprema->execute();


			 $pripremaUpit = $conn->prepare($upit);
            $pripremaUpit->bindParam(":id",$idKorisnik);
           
			 $pripremaUpit->execute();
			$sveUKorpi = $pripremaUpit->fetchAll();
			
		  echo  json_encode($sveUKorpi);

            http_response_code(200);
			
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
}