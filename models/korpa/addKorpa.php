<?php
include('../../config/connection.php');

if(isset($_POST["send"]))
{
	$idParfem=$_POST["idParfem"];
	$kolicina=$_POST["kolicina"];
	$cena=$_POST["cena"];
	$idKorisnik=$_POST["idKorisnik"];
	
	$upit="INSERT INTO korpa (IdKorisnik, IdParfem, Kolicina, UkupnaCena)
        VALUES (:idk,:idp,:kol,:uc)";
        try{
			$ukupnaCena=$cena*$kolicina;
            $priprema = $conn->prepare($upit);
            $priprema->bindParam(':idk', $idKorisnik); 
            $priprema->bindParam(':idp', $idParfem);
            $priprema->bindParam(':kol', $kolicina);
			$priprema->bindParam(':uc', $ukupnaCena);
           $rez=$priprema->execute();

		   $dohvatiBroj="SELECT COUNT(*) as BrojElemenata FROM korpa WHERE IdKorisnik=$idKorisnik AND Narucen=0";
		   $izvrsi=$conn->query($dohvatiBroj)->fetch();
		  
		   echo json_encode($izvrsi);

            http_response_code(201);
			//echo json_encode($rezultat);
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
}