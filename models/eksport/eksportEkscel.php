<?php

require_once "../../config/connection.php";


$parfemi = executeQuery("SELECT p.idParfem,p.Naziv,s.Putanja,p.Cena FROM parfem p INNER JOIN slika s ON p.idSlika=s.idSlika");

$excel = new COM("Excel.Application");


$excel->Visible = 1;

$excel->DisplayAlerts = 1;

$workbook = $excel->Workbooks->Open("http://localhost:8080/sajtPraktikumPhp/modist/models/eksport/Parfemi.xlsx") or die('Did not open filename');


$sheet = $workbook->Worksheets('Sheet1');
$sheet->activate;

$br = 1;
foreach($parfemi as $parfem){
  
    $polje = $sheet->Range("A{$br}");
    $polje->activate;
    $polje->value = $parfem->idParfem;

   
    $polje = $sheet->Range("B{$br}");
    $polje->activate;
    $polje->value = $parfem->Naziv;

  
    $polje = $sheet->Range("C{$br}");
    $polje->activate;
    $polje->value = $parfem->Putanja;

   
    $polje = $sheet->Range("D{$br}");
    $polje->activate;
    $polje->value = $parfem->Cena;

    $br++;
}


$polje = $sheet->Range("E{$br}");
$polje->activate;
$polje->value = count($parfemi);


$workbook->_SaveAs("http://localhost:8080/sajtPraktikumPhp/modist/models/eksport/Parfemi.xlsx", -4143);
$workbook->Save();


$workbook->Saved=true;
$workbook->Close;

$excel->Workbooks->Close();
$excel->Quit();

unset($sheet);
unset($workbook);
unset($excel);