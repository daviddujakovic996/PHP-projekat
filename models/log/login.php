<?php
session_start();
if(isset($_SESSION["korisnik"])){
	header("Location: ../../index.php");
}

include "../../config/connection.php";
if(isset($_POST["send"])){
	$email = $_POST["email"];
	$sifra = $_POST["sifra"];
	$siframd5 = md5($sifra);
	$validno = true;

	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$validno = false; 
	}
	if(empty($sifra)){
		$validno = false;
	}
	if($validno){
		
			$query="SELECT * FROM korisnici WHERE Email=:email AND Lozinka=:sifra";
			$priprema = $conn->prepare($query);
			$priprema->bindParam(":email", $email);
			$priprema->bindParam(":sifra", $siframd5);

			try{
			$rezultat = $priprema->execute();
			if($rezultat){
				if($priprema->rowCount()==1){
					$korisnik=$priprema->fetch();
					$_SESSION["korisnik"]=$korisnik;
					$updateKorisnik="UPDATE korisnici SET status=1 WHERE idKorisnik=:idk";
					$select=$conn->prepare($updateKorisnik);
					$select->bindParam(":idk",$_SESSION["korisnik"]->idKorisnik);
					$select->execute();
					header("Location: ../../index.php");
					http_response_code(200);
					
				}
				else{
					header("Location: ../../index.php");
				}
			}
			else{
			header("Location: ../../index.php");
				http_response_code(409);
			}
			
			}catch(PDOException $e){
			
				echo "Greska ".$e->getMessage();
			}
	}
}

?>