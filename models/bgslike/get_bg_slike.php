<?php
function get_bg_slike($slika){
	global $conn;
	
	
	 try {
	
		$upit="SELECT Putanja FROM slika WHERE NazivSlike=?";
        $select = $conn->prepare($upit);
        $select->execute([$slika]);

        $slika = $select->fetch();
        
        return $slika;
    } catch(PDOException $ex){
        return null;
    }
}

?>