<?php
session_start();
include('../../config/connection.php');

if(isset($_POST["send"]))
{
$idKorisnik=$_SESSION["korisnik"]->idKorisnik;
$upit="SELECT k.Idkorisnik,k.IdParfem,k.Kolicina,k.UkupnaCena,p.Naziv,s.Putanja,p.Cena from korpa k INNER JOIN parfem p ON k.IdParfem=p.idParfem INNER JOIN slika s ON p.idSlika=s.idSlika WHERE k.IdKorisnik=:id AND k.Narucen=1";
        try{
			
            $priprema = $conn->prepare($upit);
            $priprema->bindParam(":id",$idKorisnik);
           
			 $priprema->execute();

			$sveUKorpi = $priprema->fetchAll();
			
		   echo  json_encode($sveUKorpi);

            http_response_code(200);
			
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
}