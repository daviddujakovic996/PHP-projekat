<?php
include('../../config/connection.php');

if(isset($_POST["send"]))
{	

	$naziv=$_POST["naziv"];
	$cena=$_POST["cena"];
	$pol=$_POST["pol"];
	$brend=$_POST["brend"];
	$godinaIzdanja=$_POST["godinaIzdanja"];
	$militraza=$_POST["militraza"];
	$idParfem=$_POST["idParfem"];

	$upit="UPDATE parfem SET idBrend=:idb,Naziv=:naziv,idPol=:idp,idGodinaIzdanja=:idgi,Cena=:ce,idMilitraza=:idm WHERE idParfem=:idpa";
        try{
			
            $priprema = $conn->prepare($upit);
            $priprema->bindParam(':idb', $brend); 
            $priprema->bindParam(':naziv', $naziv);
            $priprema->bindParam(':idp', $pol);
			$priprema->bindParam(':idgi', $godinaIzdanja);
			$priprema->bindParam(':ce', $cena);
			$priprema->bindParam(':idm', $militraza);
			$priprema->bindParam(':idpa', $idParfem);
           $priprema->execute();


            http_response_code(202);
			//echo json_encode($rezultat);
			
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
}

?>