<?php
function dohvatiParfem($id){
global $conn;
	
	
 try {
 $upit = "
  SELECT p.idPol,p.idBrend,p.idGodinaIzdanja,p.idMilitraza,p.idParfem,p.Naziv,s.Putanja,p.Cena,gi.godinaIzdanja,m.vrednost,po.Naziv AS pol,b.Naziv AS brend FROM parfem p INNER JOIN slika s ON p.idSlika=s.idSlika INNER JOIN godinaizdanja gi ON p.idGodinaIzdanja=gi.idGodinaIzdanja INNER JOIN militraza m ON m.idM=p.idMilitraza INNER JOIN pol po ON p.idPol=po.idPol INNER JOIN brend b ON p.idBrend=b.idBrend WHERE p.idParfem=?
 ";
 $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $parfem = $select->fetch();
        
        return $parfem;
    } catch(PDOException $ex){
        return null;
    }

}
function militraza($id){
global $conn;
	
	
 try {
 $upit = "
	SELECT * FROM militraza WHERE idM != ?
 ";
 $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $militraza = $select->fetchAll();
        
        return $militraza;
    } catch(PDOException $ex){
        return null;
    }

}

function godina($id){
global $conn;
	
	
 try {
 $upit = "
	SELECT * FROM godinaIzdanja WHERE idGodinaIzdanja != ?
 ";
 $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $godina = $select->fetchAll();
        
        return $godina;
    } catch(PDOException $ex){
        return null;
    }

}

function brend($id){
global $conn;
	
	
 try {
 $upit = "
	SELECT * FROM brend WHERE idBrend != ? 
 ";
 $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $brend = $select->fetchAll();
        
        return $brend;
    } catch(PDOException $ex){
        return null;
    }

}
function pol($id){
global $conn;
	
	
 try {
 $upit = "
	SELECT * FROM pol WHERE idPol != ?
 ";
 $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $pol = $select->fetchAll();
        
        return $pol;
    } catch(PDOException $ex){
        return null;
    }

}
?>