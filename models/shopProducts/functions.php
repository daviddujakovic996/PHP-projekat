<?php




function rand_proizvodi(){
global $conn;
	
	
	 try {
		$upit="SELECT p.idParfem,p.Naziv,p.Cena,s.Putanja FROM parfem p INNER JOIN slika s ON p.idSlika=s.idSlika
			ORDER BY RAND()
			LIMIT 6";
			 $select = $conn->prepare($upit);
        $select->execute();

        $rand = $select->fetchAll();
        
        return $rand;
    } catch(PDOException $ex){
        return null;
    }
}

function brends($par1,$par2){
	global $conn;
	
	
	 try {
	
		$upit="SELECT DISTINCT b.Naziv,b.idBrend FROM brend b INNER JOIN parfem p ON b.idBrend=p.idBrend WHERE (p.idPol=? OR p.idPol=?) AND p.status=0";
        $select = $conn->prepare($upit);
		$select->bindValue(1,$par1);
		$select->bindValue(2,$par2);
        $select->execute();

        $brendovi = $select->fetchAll();
        
        return $brendovi;
    } catch(PDOException $ex){
        return null;
    }
}


function militraza($par1,$par2){
	global $conn;
	
	
	 try {
	
		$upit="SELECT DISTINCT m.idM,m.vrednost FROM militraza m INNER JOIN parfem p ON m.idM=p.idMilitraza WHERE (p.idPol=? OR p.idPol=?) AND p.status=0";
        $select = $conn->prepare($upit);
		$select->bindValue(1,$par1);
		$select->bindValue(2,$par2);
        $select->execute();

        $brendovi = $select->fetchAll();
        
        return $brendovi;
    } catch(PDOException $ex){
        return null;
    }
}

function godina($par1,$par2){
	global $conn;
	
	
	 try {
	
		$upit="SELECT DISTINCT g.idGodinaIzdanja,g.godinaIzdanja FROM godinaizdanja g  INNER JOIN parfem p ON g.idGodinaIzdanja=p.idGodinaIzdanja WHERE (p.idPol=? OR p.idPol=?) AND p.status=0";
        $select = $conn->prepare($upit);
		$select->bindValue(1,$par1);
		$select->bindValue(2,$par2);
        $select->execute();

        $godina = $select->fetchAll();
        
        return $godina;
    } catch(PDOException $ex){
        return null;
    }
}

function shop_products(){
	global $conn;
	
	
	 try {
	
		$upit="SELECT p.idParfem,p.Naziv,s.Putanja,p.Cena,p.brojKupovina FROM parfem p INNER JOIN slika s ON p.idSlika=s.idSlika ORDER BY p.brojKupovina DESC LIMIT 8";
        $select = $conn->prepare($upit);
        $select->execute();

        $shopProducts = $select->fetchAll();
        
        return $shopProducts;
    } catch(PDOException $ex){
        return null;
    }
}
function get_single_product($id){
	global $conn;
	
	
	 try {
	
		$upit="SELECT p.idParfem,p.Naziv,s.NovaSlika,s.Putanja,p.Cena,p.brojKupovina,g.godinaIzdanja,m.vrednost,po.Naziv as Pol FROM parfem p INNER JOIN slika s ON p.idSlika=s.idSlika INNER JOIN godinaizdanja g ON p.idGodinaIzdanja=g.idGodinaIzdanja INNER JOIN militraza m ON p.idMilitraza=m.idM INNER JOIN pol po ON p.idPol=po.idPol WHERE p.idParfem=?";
        $select = $conn->prepare($upit);
		$select->bindValue(1,$id);
        $select->execute();

        $product = $select->fetch();
        
        return $product;
    } catch(PDOException $ex){
        return null;
    }
}


?>