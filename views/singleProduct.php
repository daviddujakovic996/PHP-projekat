
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">Proizvod</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Pocetna</a></span> <span class="mr-2"><a href="index.php?page=Shop">Proizvodi</a></span> <span>Pojedinacan proizvod</span></p>
          </div>
        </div>
      </div>
    
		<?php
		require_once "models/shopProducts/functions.php";
		$idParfem=$_GET["product"];
		$parfem=get_single_product($idParfem);
		
		?>
		<section class="ftco-section bg-light">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="assets/images/nova_<?= $parfem->NovaSlika; ?>" id="slikaNova" class="image-popup"><img src="assets/images/<?= $parfem->Putanja; ?>" id="slika" class="img-fluid" alt="Colorlib Template"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3><?= $parfem->Naziv; ?></h3>
    				<p class="price"><span><?= $parfem->Cena; ?> RSD</span></p>
					<input type="hidden" id="skriveno" value="<?= $parfem->idParfem; ?>"/>
					<input type="hidden" id="cena" value="<?= $parfem->Cena; ?>"/>
    				<table border='1'>
					  <tr>
						<td>Godina Izdanja</td>
						<td>Militraza</td>
						<td>Pol</td>
					  </tr>
					  <tr>
						<td><?= $parfem->godinaIzdanja; ?></td>
						<td><?= $parfem->vrednost; ?></td>
						<td><?= $parfem->Pol; ?></td>
					  </tr>
					  
					</table>
						<div class="row mt-4">
							<div class="col-md-6">
								<div class="form-group d-flex">
		              <div class="select-wrap">
	                 
	                  
	                </div>
		            </div>
							</div>
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	             	<span class="input-group-btn mr-2">
	                	
	                   
	                	
	            		</span>
	             	KOLICINA<input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
	             	<span class="input-group-btn ml-2">
	                	
	                     
	                
	             	</span>
	          	</div>
          	</div>
			<?php 
			if(isset($_SESSION["korisnik"])){
			?>
          	<p><a href="#" class="btn btn-primary py-3 px-5" id="korpa" data-id="<?php echo $_SESSION["korisnik"]->idKorisnik; ?>">Dodaj u korpu</a></p>
			<?php
			}else{
			?>
			<p><a href="index.php?page=Moj Nalog" class="btn btn-primary py-3 px-5" >Dodaj u korpu</a></p>

			<?php
			};
			?>
    		</div>
    		</div>
    	</div>
    </section>

		