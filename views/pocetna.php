		<?php
	
		
                  include "models/bgslike/get_bg_slike.php";
				  
				
                  $slika = get_bg_slike($bgslika);
                  if($slika == null) 
                  {
                    header("Location: 404.php");
                  }else{
				  
				  
                ?>
		<div class="hero-wrap js-fullheight" style="background-image: url('assets/images/<?= $slika->Putanja; ?>');">
		<?php } ?>
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
        	<h3 class="v">Lux-Parfemi </h3>
        	<h3 class="vr">Od - 1995</h3>
          <div class="col-md-11 ftco-animate text-center">
            <h1>Lux-Parfemi</h1>
            <h2 style="color:black"><span>Izaberi Svoj Parfem</span></h2>
          </div>
          <div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-down"></span></div>
						</a>
					</div>
        </div>
      </div>
    </div>

    <div class="goto-here"></div>
    
    <section class="ftco-section ftco-product">
    	<div class="container">
    		<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<h1 class="big">Proivodi</h1>
            <h2 class="mb-4">Proivodi</h2>
          </div>
        </div>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="product-slider owl-carousel">

						<?php 
						include "models/shopProducts/functions.php";
				  
						$najpopularnije=rand_proizvodi();
						

						if($najpopularnije == null) 
						{
							header("Location: 404.php");
						}else{
							foreach($najpopularnije as $n):
							
						?>
    					<div class="item">
		    				<div class="product">
		    					<a href="index.php?page=singleProduct&product=<?= $n->idParfem; ?>" class="img-prod"><img class="img-fluid" src="assets/images/<?= $n->Putanja; ?>" alt="<?=  $n->Naziv; ?>">
		    						
									
									
		    					</a>
		    					<div class="text pt-3 px-3">
		    						<h3><a href="index.php?page=singleProduct&product=<?= $n->idParfem; ?>"><?=  $n->Naziv; ?></a></h3>
		    						<div class="d-flex">
		    							<div class="pricing">
			    							<p class="price">
											<span class="price-sale">
											<?php 
											
												echo $n->Cena . "RSD</br>";
											
											?></span></p>
			    						</div>
			    						<div class="rating">
			    							
			    						</div>
		    						</div>
		    					</div>
		    				</div>
	    				</div>
						<?php endforeach; } ?>
	    				
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
			<div class="container">
			
				<div class="row">
				<?php
				 if($slika == null) 
                  {
                    header("Location: 404.php");
                  }else{
				  
				  
                ?>
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url(assets/images/<?= $slika->Putanja; ?>);">
					<?php }; ?>
						<a href="https://www.youtube-nocookie.com/embed/4NXbiGTmbrc" class="icon popup-vimeo d-flex justify-content-center align-items-center">
							<span class="icon-play"></span>
						</a>
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-5 mt-md-5">
	          	<div class="ml-md-0">
		
		            <h2 class="mb-4">Lux-Parfemi <br>Online <br> <span> Shop</span></h2>
	            </div>
	          
	          <div class="pb-md-5">
			 
							<p>Lux-Parfemi je mesto broj jedan na internetu za kupovinu parfema, po najpovoljnijim cenama. Svakodnevno imamo preko 3.000 parfema na zalihama, koje dostavljamo na adresu u roku od dva radna dana. Ogromna ponuda parfema koju nudimo za raliku od bilo koje tradicionalne prodavnice.</p>
							<p>Jedini koji nudimo da kupujete originalne parfeme preko interneta, u Srbiji, Lux-Parfemi je mesto za vas.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

    

    


    

   