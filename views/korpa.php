<?php 
if(isset($_SESSION["korisnik"])){
?>
		
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">Moja korpa</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.php">Home</a></span> <span>Cart</span></p>
          </div>
        </div>
      </div>
    
		
		<section class="ftco-section ftco-cart">
			<div class="container">
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>&nbsp;</th>
						        <th>&nbsp;</th>
						        <th>Proizvod</th>
						        <th>Cena</th>
						        <th>Kolicina</th>
						        <th>Ukupno</th>
						      </tr>
						    </thead>
						    <tbody>
						     <div id="ispis"></div>
						    </tbody>
						  </table>
					  </div>
    			</div>
    		</div>
    		<div class="row justify-content-end">
    			<div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
    				<div class="cart-total mb-3">
    					<h3>Ukupno u korpi</h3>
    					<p class="d-flex">
    						<span>Ukupno</span>
    						<span id="zbir"></span>
    					</p>
						<hr>
    					
    				</div>
    				<p class="text-center"><a href="#" id="naruci" class="btn btn-primary py-3 px-4">Naruci</a></p>
    			</div>
    		</div>
			</div>
		</section>

    

<?php
}else{
	header("Location:index.php");
}

?>