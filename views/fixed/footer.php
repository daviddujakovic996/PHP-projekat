<footer class="ftco-footer bg-light ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Lux-parfemi</h2>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://twitter.com/?lang=sr"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/?hl=sr"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Menu</h2>
              <ul class="list-unstyled">
			  <?php 
			  require_once "models/menu/functions.php";
			  $nav=get_menu();
			  foreach($nav as $n):
			  if($n->Naziv!="Korpa"){
			  ?>
                <li><a href="index.php?page=<?= $n->Naziv ?>" class="py-2 d-block"><?= $n->Naziv ?></a></li>
				<?php }else{ if(isset($_SESSION["korisnik"])){
				 ?>
				 <li><a href="index.php?page=<?= $n->Naziv ?>" class="py-2 d-block"><?= $n->Naziv; ?></a></li>
				 <?php
				} }
				endforeach; ?>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Info</h2>
              <div class="d-flex">
	              <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
	                <li><a href="index.php?page=Autor" class="py-2 d-block">Autor</a></li>
	                <li><a href="Dokumentacija.pdf" class="py-2 d-block">Dokumentacija</a></li>
	               
	              </ul>
	              
	            </div>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Imate pitanje?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Petra Pana 17, Beograd, Srbija</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+38164 019 63 55</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text"> david.dujakovic.342.17@ict.edu.rs</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						  Copyright &copy;<script>document.write(new Date().getFullYear());</script> 
						  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.easing.1.3.js"></script>
  <script src="assets/js/jquery.waypoints.min.js"></script>
  <script src="assets/js/jquery.stellar.min.js"></script>
  <script src="assets/js/owl.carousel.min.js"></script>
  <script src="assets/js/jquery.magnific-popup.min.js"></script>
  <script src="assets/js/aos.js"></script>
  <script src="assets/js/jquery.animateNumber.min.js"></script>
  <script src="assets/js/bootstrap-datepicker.js"></script>
  <script src="assets/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="assets/js/google-map.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/filter.js"></script>
   <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="Shop"){
   ?>
      <script src="assets/js/filterParfem.js"></script>
   <?php
   }

   ?>
    <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="Moj Nalog" AND !isset($_SESSION["korisnik"])){
   ?>
      <script src="assets/js/loginRegistracija.js"></script>
   <?php
   }

   ?>

    <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="Moj Nalog" AND isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"]->idUloga=="2"){
   ?>
      <script src="assets/js/naruceniProizvodi.js"></script>
   <?php
   }

   ?>
     <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="Moj Nalog" AND isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"]->idUloga=="1" AND !isset($_GET["idUpdate"])){
   ?>
      <script src="assets/js/admin.js"></script>
   <?php
   }

   ?>
      <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="Moj Nalog" AND isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"]->idUloga=="1" AND isset($_GET["idUpdate"])){
   ?>
      <script src="assets/js/updateParfem.js"></script>
   <?php
   }

   ?>
    <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="singleProduct"){
   ?>
      <script src="assets/js/korpa.js"></script>
   <?php
   }

   ?>
   <?php
   if(isset($_GET["page"]) AND $_GET["page"]=="Korpa"){
   ?>
      <script src="assets/js/PravaKorpa.js"></script>
   <?php
   }

   ?>
  </body>
</html>