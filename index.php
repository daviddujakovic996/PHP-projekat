<?php
session_start();
//var_dump($_SESSION["korisnik"]);
require_once "config/connection.php";

  include "views/fixed/head.php";
  
  include "views/fixed/nav.php";
  

  if(!isset($_GET['page'])){
    $bgslika="pocetna";
    include "views/pocetna.php";
	
  }
  else {
    switch($_GET['page']){
      case 'Shop':
	    $bgslika="shop";
        include "views/shop.php";
		break;
      case 'Moj Nalog':
	    $bgslika="registracija";
		if(!isset($_SESSION["korisnik"])){
        include "views/registracijaLogin.php";
		}else{
		if($_SESSION["korisnik"]->idUloga=="2"){
			include "views/MojNalog.php";
			}elseif($_SESSION["korisnik"]->idUloga=="1"){
			include "views/Admin.php";
			}
		}
		break;
      case 'singleProduct':
		
        include "views/singleProduct.php";
        break;
	   case 'Korpa':
	    
		if(!isset($_SESSION["korisnik"])){
        include "views/registracijaLogin.php";
		}else{
			include "views/korpa.php";
		}
		break;
	    
		case 'Autor':
			include "views/autor.php";
			break;
      default: 
	    $bgslika="pocetna";
        include "views/pocetna.php";
        break;
    }
  }

  include "views/fixed/footer.php";
